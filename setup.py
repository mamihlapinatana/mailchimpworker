import os
from setuptools import find_packages, setup

with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
    README = readme.read()

os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(name='mailchimpworker',
      version='0.18',
      description='Mailchimpworker',
      author='A. Igin',
      url='http://mamihlapinatana.ru',
      )
