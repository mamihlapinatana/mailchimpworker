from django.core.management import call_command
from nosorog.celery import app


@app.task
def add_subscribers(*args, **kwargs):
    return call_command('add_subscribers')
